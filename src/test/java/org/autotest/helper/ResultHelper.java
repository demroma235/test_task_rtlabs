package org.autotest.helper;

import org.openqa.selenium.WebElement;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ResultHelper {
    public Integer getIntFromStringStarsFromList(WebElement item) {
        String stars_string = item.getText();
        String stars = stars_string.substring(0, stars_string.length() - 1);
        float stars_float = Float.parseFloat(stars);
        return (int) (stars_float * 1000);
    }

    public String getUsernameByString(String full_string){
        String[] list_name = full_string.split("/");
        return list_name[0];
    }
}
