package org.autotest.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ResultPage {

    private final WebDriver driver;
    private By item_star = By.xpath("//a[@class=\"muted-link\"]");
    private By item_language = By.xpath("//*[@class=\"repo-list\"]/div/div[2]");
    private By item_author = By.xpath("//a[@class=\"v-align-middle\"]");

    private HashMap<String, By> language_to_by_map = new LinkedHashMap<>();

    public ResultPage(WebDriver driver) {
        this.driver = driver;
        language_to_by_map.put(
                "JavaScript",
                By.xpath("//*[@id=\"js-pjax-container\"]/div/div/div[1]/div[1]/ul/li[3]/a"
                )
        );
    }

    public List<WebElement> getResultStars() {
        return driver.findElements(item_star);
    }

    public ResultPage clickByLanguage(String lang) throws InterruptedException {
        By path = language_to_by_map.get(lang);
        driver.findElement(path).click();
        Thread.sleep(1000);
        return this;
    }

    public List<WebElement> getResultAuthors() {
        return driver.findElements(item_author);
    }

    public List<WebElement> getResultLanguage() {
        return driver.findElements(item_language);
    }
}
