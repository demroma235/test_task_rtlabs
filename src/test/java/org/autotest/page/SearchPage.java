package org.autotest.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchPage {

    private final WebDriver driver;
    private String url = "https://github.com/search";

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        driver.get(this.url);
    }

    private By search_field = By.xpath("//input[@type=\"text\"]");
    private By search_button = By.xpath("//button[@type=\"submit\"]");

    public ResultPage searchResult(String search_text) {
        driver.findElement(search_field).sendKeys(search_text);
        driver.findElement(search_button).click();

        return new ResultPage(driver);
    }
}
