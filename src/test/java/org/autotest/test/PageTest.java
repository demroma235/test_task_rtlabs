package org.autotest.test;

import org.autotest.base.BaseTest;
import org.autotest.helper.ResultHelper;
import org.autotest.page.ResultPage;
import org.autotest.page.SearchPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

public class PageTest extends BaseTest {

    @DataProvider
    public Object[][] parseAuthorLang() {
        return new Object[][]{
                {"defunkt", "JavaScript"}
        };
    }

    @DataProvider
    public Object[][] parseStars() {
        return new Object[][]{
                {10000}
        };
    }

    @Test(dataProvider = "parseStars")
    public void checkStars(Integer min_stars) {
        String search_star = "test stars:>";

        SearchPage search_page = new SearchPage(driver);
        ResultPage resultPage = search_page.searchResult(search_star + min_stars);
        List<WebElement> items = resultPage.getResultStars();
        ResultHelper resultHelper = new ResultHelper();
        for (WebElement webElement : items) {
            if (min_stars > resultHelper.getIntFromStringStarsFromList(webElement)) {
                Assert.fail();
            }
        }
    }

    @Test(dataProvider = "parseAuthorLang")
    public void checkAuthorAndLang(String author, String lang) throws InterruptedException {
        SearchPage search_page = new SearchPage(driver);
        String search_request = "user:" + author;
        ResultPage resultPage = search_page.searchResult(search_request);
        resultPage.clickByLanguage(lang);
        List<WebElement> authors = resultPage.getResultAuthors();
        ResultHelper resultHelper = new ResultHelper();
        for (WebElement webElement: authors) {
            if(!resultHelper.getUsernameByString(webElement.getText()).equals(author)){
                Assert.fail();
            }
        }
        List<WebElement> languages = resultPage.getResultLanguage();
        for (WebElement webElement: languages) {
            if(!webElement.getText().equals(lang)){
                Assert.fail();
            }
        }
    }
}